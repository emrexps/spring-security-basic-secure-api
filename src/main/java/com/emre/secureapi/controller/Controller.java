package com.emre.secureapi.controller;

import java.security.Principal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@GetMapping("/nonsecure")
	public String nonsecureApi() {
		return "this is non secure";
	}
	
	@GetMapping("/secure")
	public String secureApi(Principal principal) {
		return "this is secure "+principal.getName();
	}

}
